from django.db import models


class DAutodismantling(models.Model):
    name = models.CharField(max_length=100)

    class Meta:
        db_table = 'dAutodismantling'


class ParameretTypes(models.Model):
    name = models.CharField(max_length=100)

    class Meta:
        db_table = 'parameretTypes'


class Params(models.Model):
    name = models.CharField(max_length=100)
    param_type = models.ForeignKey(ParameretTypes)
    param = models.ForeignKey(DAutodismantling)

    class Meta:
        db_table = 'params'



