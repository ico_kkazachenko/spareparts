from django.apps import AppConfig


class SparepartsConfig(AppConfig):
    name = 'SpareParts'
