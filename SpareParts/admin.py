from django.contrib import admin

# Register your models here.
from SpareParts.models import DAutodismantling, ParameretTypes, Params

admin.site.register([DAutodismantling, ParameretTypes, Params])